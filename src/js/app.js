(function() {

  const navigation = document.getElementById( 'navigation' );
  const searchOpen = document.getElementById( 'search-open' );
  const searchClose = document.getElementById( 'search-close' );

  searchOpen.addEventListener( 'click', () => {
    navigation.classList.add( 'nav--search-active' );
  });

  searchClose.addEventListener( 'click', () => {
    navigation.classList.remove( 'nav--search-active' );
  });

}());
